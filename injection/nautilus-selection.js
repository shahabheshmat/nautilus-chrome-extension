const nautilusData = {
    collectedElements: new Set(),
    adapter: undefined,
    isCollectingData: false,
    lastHoveredElement: undefined,
    IGNORED_ELEMENT_TYPES: ['button', 'html', 'body'],
    addContent(content) {
        this.collectedElements.add(content);

        if (this.adapter) {
            this.adapter(content);
        }
    },
    convertElementToNautilusData(element) {
        const elementType = element.tagName.toLowerCase();
        switch (elementType) {
            case 'audio':
            case 'video':
                return {
                    type: elementType,
                    details: {
                        src: element.src
                    }
                };
            case 'img':
                return {
                    type: 'image',
                    details: {
                        src: element.src,
                        alt: element.alt
                    }
                };

            default:
                return {
                    type: 'text',
                    details: {
                        content: element.innerText
                    }
                };
        }
    },
    onElementClick(event) {
        if (
            this.isCollectingData &&
            this.IGNORED_ELEMENT_TYPES.indexOf(event.target.tagName.toLowerCase()) === -1
        ) {
            const content = this.convertElementToNautilusData(event.target);
            this.addContent(content);
        }
    },
    onElementHover(event) {
        if (!this.isCollectingData) {
            return;
        }
        if (this.lastHoveredElement && this.lastHoveredElement !== event.target) {
            this.lastHoveredElement.classList.remove('nautilus-hovered-element');
        }
        event.target.classList.add('nautilus-hovered-element');
        this.lastHoveredElement = event.target;
    },
};

addEventListener('mousemove', function (event) {
    nautilusData.onElementHover(event);
});
addEventListener('click', function (event) {
    nautilusData.onElementClick(event);
});

function startCollecting(adapter) {
    console.log("ch khabar?");
    nautilusData.isCollectingData = true;
    nautilusData.adapter = adapter;
}

function stopCollecting() {
    nautilusData.isCollectingData = false;
}
