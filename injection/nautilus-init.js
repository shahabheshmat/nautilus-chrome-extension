const selectionScriptUrl = chrome.runtime.getURL('injection/nautilus-selection.js');
const scriptElement = document.createElement('script');
scriptElement.src = selectionScriptUrl;

const documentElement = document.head || document.documentElement;

documentElement.appendChild(scriptElement);