const messagePortName = 'content-basket';

const protocol = {
    extension: {
        REQUEST_CONTENT: 'Start giving me content.',
        STOP: 'Stop sending content.'
    },
    window: {
        ADD_CONTENT: 'Please add this content.',
    }
};

function buildMessage(message, attachment = {}) {
    return {
        sentence: message,
        ...attachment
    };
}

function checkMessage(message, sentence) {
    return message.sentence === sentence;
}

const messagePorts = {
    // [tabId]: messagePort
};

function startListeningForMessagesInTab(tabId) {
    chrome.scripting.executeScript(
        {
            target: {
                tabId
            },
            function() {
                console.log("salame garm")
                chrome.runtime.onConnect.addListener(function (port) {
                    if (port.name === messagePortName) {
                        function addElement(content) {
                            const message = buildMessage(protocol.window.ADD_CONTENT, {
                                details: content
                            });

                            port.postMessage(message);
                        }

                        port.onMessage.addListener(function (message) {
                            if (checkMessage(message, protocol.extension.REQUEST_CONTENT)) {
                                /* global startCollecting */
                                startCollecting(addElement);
                            } else if (checkMessage(message, protocol.extension.STOP)) {
                                /* global stopCollecting */
                                stopCollecting();
                            }
                        });
                    }
                });
            },
        });
}

function createMessagePortFor(tabId) {
    if (!messagePorts[tabId]) {
        messagePorts[tabId] = chrome.tabs.connect(tabId, {
            name: messagePortName,
        });

        startListeningForMessagesInTab(tabId);
    }

    return messagePorts[tabId];
}