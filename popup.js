/* global messagePortName */
/* global protocol */
/* global buildMessage */
/* global checkMessage */
/* global createMessagePortFor */

const content = [];
let activeTabId;
let isCollecting = false;
let messagePort;

const collectContentButton = document.getElementById('collect-content-button');
const collectContentButtonTexts = {
    startCollecting: 'Start Collecting',
    stopCollecting: 'Stop Collecting'
};

async function toggleCollect() {
    if (!activeTabId) {
        return;
    }

    isCollecting = !isCollecting;

    if (isCollecting) {
        collectContentButton.innerText = collectContentButtonTexts.stopCollecting;

        const message = buildMessage(protocol.extension.REQUEST_CONTENT);
        messagePort.postMessage(message);

    } else {
        collectContentButton.innerText = collectContentButtonTexts.startCollecting;

        const message = buildMessage(protocol.extension.STOP);
        messagePort.postMessage(message);
    }
}

async function setup() {
    const [tab] = await chrome.tabs.query({
        active: true,
        currentWindow: true
    });

    activeTabId = tab.id;

    messagePort = createMessagePortFor(activeTabId);

    messagePort.onMessage.addListener(function (message) {
        if (checkMessage(message, protocol.window.ADD_CONTENT)) {
            content.push(message.details.type);
        }
    });

    chrome.scripting.executeScript({
        target: {
            tabId: activeTabId,
        },
        files: ['./protocol.js']
    });
}

collectContentButton.addEventListener('click', toggleCollect);

const contentElement = document.getElementById('content');

const showContentButton = document.getElementById('show-content-button');
showContentButton.addEventListener('click', function () {
    contentElement.innerHTML = content.join('\n<br />\n');
});

setup();